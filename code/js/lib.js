$(document).ready(function () {

    /**
    * gibt eine Funktion zurück, die das StudentJson in eine datei speichert
    * und herunterlädt. 
    */
    exportStudent = (function () {
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        return function (data, fileName) {
            seen = [];
            var json = JSON.stringify(data, function (key, val) {
                if (typeof val == "object") {
                    if (seen.indexOf(val) >= 0)
                        return
                    seen.push(val)
                }
                return val
            })
            blob = new Blob([json], { type: "octet/stream" }),
                url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = fileName;
            a.click();
            window.URL.revokeObjectURL(url);
        };
    }());

    /**
   * überprüfe die Eingabedatei.
   * inhalt der Datei muss ein json sein.
   * @param {file} fileinput 
   */
    validateImpotedFile = function (fileinput, student, steps) {
        var selectedFile = fileinput.files[0];
        var filename = selectedFile.name;
        arr = filename.split(".");
        if (arr[1] !== 'json') {
            alert("please only files with '.json' extension ");
            return
        }
        var data = null;
        var isValid = true;
        readFile(selectedFile, function (fileContent) {
            if (fileContent !== undefined) {
                try {
                    data = JSON.parse(fileContent)
                } catch (e) {
                    alert("content of the uploaded file is not a json object");
                    isValid = false;
                }
            } else {
                alert("can't read the uploaded file");
                isValid = false;
            }
            if (isValid) {
                var parser = new ImportParser(data, student, steps);
                parser.init();
            } else {
                return
            }
        });
    }



    /**
     * Datei lesen und callback aufrufen
     * @param {file} file 
     * @param {function} cb 
     */
    readFile = function (file, cb) {
        var reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = function (evt) {
            content = evt.target.result;
            cb(content);
        }
        reader.onerror = function (evt) {
            cb();
        }
    }


    ziehenNachErstemStep = function () {
        var $active = $('.formularWrapper .nav-tabs li.active');
        var $first = $('.formularWrapper .nav-tabs li').first();
        $active.removeClass('active');
        $first.find('a[data-toggle="tab"]').click();
    }


    /**
     * wächsle zum nächsten Step und aktualisiere den Prograssbar entsperchend 
     */
    oeffneNaechsterStep = function () {
        var $active = $('.formularWrapper .nav-tabs li.active');
        $active.next().removeClass('disabled');
        $active.next().find('a[data-toggle="tab"]').click();
        $active.next().addClass('opend');
        tabsProgress();
    }

    /**
     * aktualiere den Progressbar
     */
    tabsProgress = function () {
        var num = $('.formularWrapper .nav-tabs li.opend').length - 1;
        var toValue = 25 * num;
        $(".tabsProgress > div").css({ 'background': 'linear-gradient(135deg, #314b62, #314b6240)' });
        $(".tabsProgress > div").css({ "width": "" + toValue + "%" });

    }

    /**
     * alle steps > stepNr werden geschlossen. 
     * @param {int} stepNr 
     */
    schliesseNaechsteSteps = function (stepNr) {
        $('.formularWrapper .nav-tabs li').slice(stepNr).addClass('disabled');
        $('.formularWrapper .nav-tabs li').slice(stepNr).removeClass('opend');
        tabsProgress();
    }

    /**
     * @param {int} stepNr 
     */
    openSteps = function (stepNr) {
        $('.formularWrapper .nav-tabs li').slice(1, stepNr).removeClass('disabled').addClass('opend');
        tabsProgress();
    }


    /**
     * Sprache wird geändert.
     * benutzt die Sprachmap in sprachen.json   
     * @param {string} selectedlanguage 
     */
    uebersetzeZu = function (selectedlanguage) {
        $.getJSON("data/sprachen.json", function (data) {
            $.each(data[selectedlanguage], function (key, val) {
                var list = $('[data-lang]');
                $.each(list, function () {
                    if (key == $(this).data("lang")) {
                        $(this).html(val);
                    }
                });
            });
        });
        $("[data-switch-lang]").each(function () {
            // temp=a, a=b,b=temp 
            var text = $(this).text();
            $(this).text($(this).attr("data-switch-lang"));
            $(this).attr("data-switch-lang", text);
        });
    }

    zeigeSumLP = function (lpSumme, akzeptierteLpSumme) {
        if (getSelectedSprache() === 0) { 
            $('#summe').text('ETCS of ' + lpSumme + ' ETCS can be transferred.').prepend('<span class="neueSummeLp" >' + akzeptierteLpSumme + '</span> ');
        } else {
            $('#summe').text('ETCS von ' + lpSumme + ' ETCS  können anerkannt werden.').prepend('<span class="neueSummeLp" >' + akzeptierteLpSumme + '</span> ');
        }
    }
    
    zeigeStudentinfos = function (vorname, nachname, adresse, email) {
        var studiengang = $("#studiengang").find(":selected").text()
        if (getSelectedSprache() === 0) {
            $('#stundentInfos').text('Transferable modules for the degree program ' +  studiengang + ' for ').append('<br><span class="studentName" >' + vorname+ ' ' + nachname + '</span> <br><span class="studentName" >' + emaile + '</span> <br><span class="studentName" >' + adress + '</span>');
        } else {
            $('#stundentInfos').text('Anerkennungen für den Studiengang ' + studiengang + ' für ').append('<br><span class="studentName" >' + vorname+ ' ' + nachname + '</span> <br><span class="studentName" >' + email + '</span> <br><span class="studentName" >' + adresse + '</span>');
        }
    }

    getSelectedSprache = function () {
        var selectedlanguage = $('.SpracheWechseln .aktuelleSprache').text();
        if (selectedlanguage === "EN") {
            return 0;
        } else {
            return 1;
        }
    }


});




