$(document).ready(function () {

    addGlobalListeners = function () {
        /**
         * export student als json datei
         */
        $('#dataExport').click(function () {
            if (student !== null) {
                var data = student.getJson();
                data["sprache"] = getSelectedSprache() === 1 ? "DE" : "EN";
                student.getNachname !== "" ? fileName = student.getNachname() + ".json" : fileName = "myData.json";
            } else {
                var data = {};
                data.fehler = "keine Daten eigegeben";
                fileName = "myData.json";
            }
            exportStudent(data, fileName);

        });

        /**
         * Sprachwechsel handlen
         * löscht die alte Optionen von Länder und Noten und
         * baut auch die neue Ländernamen in der ausgewählten Sprache
         * ein (Studiengänge und Module nicht).   
        */
        $(" .SpracheWechseln li > a").click(function () {
            if ($("#finalTabelle tr.active").length > 0) {
                $("#finalTabelle tr.active td > a.addChangeButton").click()
            }
            var selectedlanguage = $(this).find('span').text();
            if (selectedlanguage === (getSelectedSprache() === 1 ? "DE" : "EN")) return
            uebersetzeZu(selectedlanguage);
            $(".SpracheWechseln a > span.aktuelleSprache").text(selectedlanguage);
            zeigeSumLP(student.getLpSumme(), student.getAkzeptierteLpSumme());
            zeigeStudentinfos(student.getVorname(), student.getNachname(), student.getAdresse(), student.getEmail());
            step3.updateTooltip();
        });

        $('#dataImport').click(function () {
            if ($('#notToDisplay').length === 0) {
                $("body").append("<input id='notToDisplay' type=file></input>");
                $('#notToDisplay').hide()
                $('#notToDisplay').change(function () {
                    step4 = step3.getStep4();
                    steps = [step1, step2, step3, step4, step4.getStep5()]
                    validateImpotedFile(this, student, steps);
                });
            }
            $('#notToDisplay').click();
        })
        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            var $target = $(e.target);
            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });
    }

    var student = new Student();
    var step1 = new Step1(student);
    step1.init();
    var step2 = new Step2(student);
    step2.init();
    var step3 = new Step3(student);
    step3.init();

    addGlobalListeners();
    // TODO: 
    $("#tabsProgress").progressbar({
        value: 0,
        max: 100
    });

    if (window.location.href.indexOf('language=en') != -1) {
        uebersetzeZu("EN");
        $(".SpracheWechseln a > span.aktuelleSprache").text("EN");
        zeigeSumLP(student.getLpSumme(), student.getAkzeptierteLpSumme());
        zeigeStudentinfos(student.getVorname(), student.getNachname(), student.getAdresse(), student.getEmail());
        step3.updateTooltip();
    }
});
