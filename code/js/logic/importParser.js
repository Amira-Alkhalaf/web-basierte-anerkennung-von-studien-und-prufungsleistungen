
function ImportParser(data_json, _student,_steps) {

    this.data = data_json;
    var that = this;
    var student = _student;
    var step1;
    var step2;
    var step3;
    var step4;
    var step5;
    var toStep;



    this.init = function () {
        var sprache = this.data["sprache"]==="DE"?1:0;            
        updateStudent();
        /* initialisierung der neuen Objekte*/
        step1 = steps[0];
        step2 = steps[1];
        step3 = steps[2];
        step4 = steps[3];
        step5 = steps[4];
        step1.addListeners();
        step1.DOMUpdate();
        step2.DOMUpdate();
        
        // update step 3 und step 4
        if (student.getBelegteModule().length > 0) {
            step3.deleteRows();
            step3.DOMUpdate();
            step3.setCurrentNotenSystem(student.getNotenSystem()); 
        }
        step4.init(step4.DOMUpdate);
        step5.init(); 

        if(getSelectedSprache()!= sprache){
            $(".SpracheWechseln a > span.aktuelleSprache").text(sprache === 0 ?"EN":"DE");
            uebersetzeZu(this.data["sprache"]);  
            steps[2].updateTooltip();
        }
        checkStepsToOpen();
        ziehenNachErstemStep()
      //  step5.updateSummeLp();

    }

    /**
     * bis zu welchem Step wurden daten angegeben
     */
    var checkStepsToOpen= function(){
     if(!student.getNotenSystem() || !student.getLand() || !student.getStudiengang()){
        openSteps(1);
        return
     } else if(student.getBelegteModule().length === 0){
        openSteps(2);
        return
     }else if(student.getZuordnungen().length === 0){
        openSteps(3);
        return
    }else{
        openSteps(5);
        return
    }

    }


    /**
     * student wird upgedatet vom Eingabejson
     */
    var updateStudent = function () {
        data = that.data;
        student.setVorname(data["vorname"]);
        student.setNachname(data["nachname"]);
        student.setAdresse(data["adresse"]);
        student.setMatrikelNummer(data["matrikelNummer"]);
        student.setEmail(data["email"]);
        student.setLand(data["land"]);
        student.setNotenSystem(data["notenSystem"]);
        student.setAlterStudiengang(data["alterStudiengang"]);
        student.setStudiengang(data["studiengang"]);
        student.setEinstufungFachsemester(data["einstufungFachsemester"]);
        var belegteModule = []
        data["belegteModule"].forEach(function (element) {
            var belegtesmodul = new BelegtesModul(element.modul, element.note, element.lp, element.anzahlVersuche)
            belegtesmodul.init(element.id)
            belegteModule.push(belegtesmodul);
            belegtesmodul.setUmgerechneteNote(student.getNotenSystem())
        });
        student.setBelegteModule(belegteModule);
        var zuordnungen = []
        data["zuordnungen"].forEach(function (element) {
            zuordnung = new Zuordnung(new Modul(element.uniModul.name, element.uniModul.bereich, element.uniModul.lp), element.benutzerModule, element.anteile,element.status)
            zuordnungen.push(zuordnung);
        });
        student.setZuordnungen(zuordnungen);
    }

}
