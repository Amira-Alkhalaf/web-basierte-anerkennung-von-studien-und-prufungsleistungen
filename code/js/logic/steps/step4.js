
function Step4(student, step3) {
    var that = this;
    var student = student;
    var step3 = step3;
    this.studiengaenge;
    var zuordnungen = [];
    var contentChanged = true;
    var stepNr = 4;
    var step5;

    this.init = function (callback) {
        if (student.getBelegteModule() !== undefined) {
            getStudiengaenge(function (data) {
                that.studiengaenge = data
                baueAkkordeonTabelle(data);
                baueDragTabelle();
                that.addListeners();
                callback ? callback() : null;
            });
        }
    };

    this.validate = function ($elements) {
        // TODO
        return true;
    };

    this.addListeners = function () {
        $("#step4 .next-step").off("click").click(function () {
            zuordnungen = [];
            parseInputData();
            that.updateUser(zuordnungen);
            if (validiereKompletterStep()) {
                if (contentChanged || $(".nav-tabs .step5").hasClass("disabled")) {
                    var step5 = new Step5(student, step3, that);
                    step5.init();
                    contentChanged = false;
                }
                oeffneNaechsterStep();
            }
        });
        $("#step4 .removeButton").off('click', removeDroppedModul).click(removeDroppedModul);
        $("#droptable div.accordion").off('click', toggleAccordion).click(toggleAccordion);

        if ($("#droptable").find("tr").length > 0) {
            addDropModule();
        }
        $('#droptable .accordionSelectpicker').on("change", function () {
            contentChanged = true;
            schliesseNaechsteSteps(stepNr);
        });
    };


    /**
     * 
     * @param {*} zuordnungen 
     */
    this.updateUser = function (zuordnungen) {
        student.setZuordnungen(zuordnungen);
    }


    /**
     * 
     */
    this.getStudent = function () {
        return student;
    }

    /**
     * 
     * @param {*} data 
     */
    var baueAkkordeonTabelle = function (data) {

        var sprache = getSelectedSprache() === 1 ? "DE" : "EN";
        var andereSprache = getSelectedSprache() === 1 ? "EN" : "DE";
        var studiengang = data["DE"][student.getStudiengang()];
        var parent = $('#droptable');
        parent.empty();
        if (getSelectedSprache() === 1) {
            $.each(studiengang, function (key, value) {
                toAdd = $("<tr>").append($("<td>").append($("<div>").addClass('accordion').attr("name", key.toLowerCase().replace(/\s/g, '')).
                attr("data-switch-lang", data["EN"][key]).val(key).text(key)));
                parent.append(toAdd);
                var last = $("#droptable td").last();
                var list = $('<ul/>').appendTo(last).addClass("accordionInhalt");
                for (var i = 0; i < value.length; i++) {
                        list.append($("<li/>").append($("<a>").attr({"name": value[i].name.toLowerCase().replace(/\s/g, ''), 
                                                                    "data-switch-lang": data["EN"][value[i].name],
                                                                    "href": value[i].link,
                                                                    "target":"_blank"
                                                                }).val(value[i].name).text(value[i].name))
                                                                .append($("<span>").addClass("lpUniModul").text("("+ value[i].lp +" LP)")));
                }
            });
        } else {
            $.each(studiengang, function (key, value) {
                toAdd = $("<tr>").append($("<td>").append($("<div>").addClass('accordion').attr("name", key.toLowerCase().replace(/\s/g, '')).
                    attr("data-switch-lang", key ).val(key).text(data["EN"][key])));
                parent.append(toAdd);
                var last = $("#droptable td").last();
                var list = $('<ul/>').appendTo(last).addClass("accordionInhalt");
                for (var i = 0; i < value.length; i++) {
                        list.append($("<li/>").append($("<a>").attr({"name": value[i].name.toLowerCase().replace(/\s/g, ''), 
                        "data-switch-lang":value[i].name,
                        "href": value[i].link,
                        "target":"_blank"
                    }).val(value[i].name).text(data["EN"][value[i].name])).append($("<span>").addClass("lpUniModul").text("("+ value[i].lp +" LP)")));
                }
            });
        }
    }

    /**
     *
     * @param {*} cb
     */
    var getStudiengaenge = function (cb) {
        $.getJSON("data/studiengaenge.json", function (data) {
            var studiengaenge = data;
            cb(studiengaenge);
            return studiengaenge;
        });
    }

    var validiereKompletterStep = function () {
        if (student.getZuordnungen().length > 0) {
            $(".formularHeader .active span").removeClass("errorTab").addClass("erfolgTab");
            return true;
        } else {
            $(".formularHeader .active span").addClass('errorTab');
            $(".formularWrapper #step2 .inputFelder").not(".erfolgInput").not(".optionalInput").addClass("errorInput");
            $(".formularWrapper #step2 .inputFelder > span").not(".glyphicon-ok").addClass("glyphicon-remove");
            return false;
        }
    };

    /**
     * 
     */
    var toggleAccordion = function () {
        var $this = $(this)
        var accordionInhalt = $this.next(".accordionInhalt");
        $this.toggleClass('active');
        accordionInhalt.slideToggle();
    }


    /**
     * 
     */
    var removeDroppedModul = function () {
        contentChanged = true;
        $('.formularWrapper .nav-tabs li').slice(4).addClass('disabled')
        var childrnNum = $(this).closest("ul").children().size();
        if (childrnNum > 1) {
            $(this).closest('li').remove();
        } else {
            $(this).closest('ul').parent().find('a').removeClass('chosenSpan');
            $(this).closest('ul').remove();
        }
        schliesseNaechsteSteps(stepNr);
    }

    /**
     * 
     */
    var baueDragTabelle = function () {
        var table2 = document.getElementById("dragTable");
        $(table2).empty();
        for (i = 0; i < student.getBelegteModule().length; i++) {
            var x = student.getBelegteModule()[i].modul;
            var lp = student.getBelegteModule()[i].lp;
            var td1 = document.createElement("li");
            if (x.length != 0) {
                td1.innerHTML = '<div  class="redips-drag redips-clone ar dragListeItems"><span data-lp='+ lp +'>' + x + '</span></div>';
                table2.appendChild(td1);
            }
        }
        addDragTabelle();
    }

    /**
     * 
     */
    var addDragTabelle = function () {
        var tb1 = $("#dragTable div span");
        $(tb1).draggable({
            helper: "clone",
            cursor: "move",
            appendTo: "body",
            opacity: "0.35",
            revert: "invalid"
        });
    }

    /**
     * 
     */
    var addDropModule = function () {
        var tb2 = $(".accordionInhalt > li");
        tb2.droppable({
            accept: 'span',
            hoverClass: "accordionInhalt-highlight",
            drop: function (event, ui) {
                var $this = $(this);
                var draggedText = $.trim($(ui.draggable).text());
                var lp =$.trim($(ui.draggable).attr("data-lp"))
                var droppendText = $this.children(":first").text();
                $this.find("a:first").addClass('chosenSpan');
                var break_ = false;
                $list = $this.find(".ul-dropList li span");
                $list.each(function () {
                    if (draggedText == $(this).text()) {
                        break_ = true;
                    }
                })
                !break_ && baueDropListe(draggedText, $this, lp );
                that.addListeners();
                schliesseNaechsteSteps(stepNr)
            }
        });
    }


    var baueDropListe = function ($draggedText, $this,lp) {
        contentChanged = true;
        var ProzentOptionen = {
            op1: { value: 100, text: "100%" },
            op2: { value: 75, text: "75%" },
            op3: { value: 50, text: "50%" },  
            op4: { value: 25, text: "25%" }
        };
        $select = $('<select/>').addClass("accordionSelectpicker");
        if (!$this.find("ul").hasClass("ul-dropList")) {
            var list = $('<ul/>').appendTo($this).addClass("ul-dropList");
            list.append($("<li/>")
                .append($select)
                .append($("<span/>").text($draggedText).addClass("dropList_elements"))
                .append($("<span/>").text(" ("+ lp +" LP)"))
                .append($("<button/>").append($("<span>").addClass('glyphicon glyphicon-trash')).addClass("btn btn-xs removeButton")));
        } else {
            $this.find("ul").append($("<li/>")
                .append($select)
                .append($("<span/>").text($draggedText).addClass("dropList_elements"))
                .append($("<span/>").text(" ("+ lp +" LP)"))
                .append($("<button/>").append($("<span>").addClass('glyphicon glyphicon-trash')).addClass("btn btn-xs removeButton")));
        }
        if ($('.accordionSelectpicker').is(":empty")) {
            $.each(ProzentOptionen, function (i, item) {
                $select.append($('<option>', {
                    value: item.value,
                    text: item.text
                }));
            });
        }
        return $select;
    }


    var parseInputData = function () {

        var table = $("#droptable > tr ul.accordionInhalt");
        table.find('a.chosenSpan').each(function (i) {

            var uniModulName = $(this).val();
            var lp =  $(this).closest("li").find(".lpUniModul").text().replace(/[()]/g,'').split('LP')[0];
            var bereich = $(this).closest("td").find('div.accordion').val();
            var $benutzerModuleNamen = $(this).parent().find("li > span.dropList_elements");
            var $anteile = $(this).parent().find(".accordionSelectpicker option:selected");
            var uniModul = new Modul(uniModulName, bereich, lp);
            var benutzerModule = [];
            $.each($benutzerModuleNamen, function (i) {
                var benutzerModul = sucheBenutzerModule($benutzerModuleNamen[i].innerHTML);
                benutzerModule.push(benutzerModul.id);
            });
            var anteile = [];
            var status = [];
            $.each($anteile, function () {
                anteile.push(this.value);
                status.push("");
            });
            var zuordnung = new Zuordnung(uniModul, benutzerModule, anteile, status);
            zuordnungen.push(zuordnung);

        });
    }

    var sucheBenutzerModule = function (suchWort) {
        for (i = 0; i < student.getBelegteModule().length; i++) {
            benutzerModul = student.getBelegteModule()[i];
            if (benutzerModul.modul === suchWort) {
                return benutzerModul
            }
        }
    }

    var zeigeAccordionInhalt = function (context) {
        $(context).addClass("active")
        $(context).next(".accordionInhalt").show();
    }



    this.DOMUpdate = function () {
        var accordion = $('#droptable');
        zuordnungen = student.getZuordnungen();
        for (var i = 0; i < zuordnungen.length; i++) {
            var bereich = zuordnungen[i].uniModul.bereich;
            var uniModulname = zuordnungen[i].uniModul.name;
            $bereich = accordion.find("div[name='" + bereich.toLowerCase().replace(/\s/g, '') + "']");
            zeigeAccordionInhalt($bereich);
            $td = $bereich.closest("td");
            $unimodulname = $td.find("a[name='" + uniModulname.toLowerCase().replace(/\s/g, '') + "']");
            belegteModuleIds = zuordnungen[i].benutzerModule;
            for (var j = 0; j < belegteModuleIds.length; j++) {
                $li = $unimodulname.closest("li");
                var $droppendText = $li.children(":first").text();
                $li.find("a").addClass('chosenSpan');
                $select = baueDropListe(student.getBelegtesModul(belegteModuleIds[j]).modul, $li, student.getBelegtesModul(belegteModuleIds[j]).lp);
                $("#step4 .removeButton").off().click(removeDroppedModul);
                $select.val(zuordnungen[i].anteile[j]);
            }
        }
        $('#droptable .accordionSelectpicker').on("change", function () {
            contentChanged = true;
            schliesseNaechsteSteps(stepNr);
        });
        contentChanged = false;
    }

    this.getStep5 = function () {
        if (step5 === undefined) {
            return new Step5(student, step3, step4);
        }
        return step5;
    }

}





