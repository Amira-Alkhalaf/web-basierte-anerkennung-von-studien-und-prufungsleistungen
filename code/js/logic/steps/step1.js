function Step1(student) {
    var that = this;
    var student = student;
    var stepNr = 1;

    this.init = function () {
        this.addListeners();
    };

    this.getStudent = function () {
        return student;
    };

    this.addListeners = function () {

        $('#step1 input').off("change").on("change", function () {
            var $inputElem = $(this);
            var validierungsstatus = validiereEingabe($inputElem);
            parseOnInput(validierungsstatus, $inputElem);
            schliesseNaechsteSteps(stepNr);
        });

        $("#step1 .next-step").off("click").click(function () {
            if (validiereKompletterStep()) {
                oeffneNaechsterStep();
            }
        });
    };

    /** 
     * Die Methode prüft, ob alle eingegebenen Daten(Inputs) korrekt sind.
     */
    var validiereKompletterStep = function () {
        if (student.getVorname() && student.getNachname() && student.getEmail()) {
            $(".formularHeader .active span").removeClass("errorTab").addClass("erfolgTab");
            return true;
        } else {
            $(".formularHeader .active span").addClass("errorTab");
            $(".formularWrapper #step1 .pflicht").not(".erfolgInput").addClass("errorInput");
            $(".formularWrapper #step1 .pflicht span").not(".glyphicon-ok").addClass("glyphicon-remove");
            return false;
        }
    };

    /**
     * Die eingegebene Date(input) wird im Student-Objekt gespeichert.
     * 
     * @param  {boolean} validierungsstatus
     * @param  {$} $inputElem
     */
    var parseOnInput = function (validierungsstatus, $inputElem) {
        var name = $inputElem.attr("name");
        name = name.charAt(0).toUpperCase() + name.slice(1);
        if (validierungsstatus) {
            student["set" + name]($inputElem.val());
        } else {
            student["set" + name]("");
        }
    };


    /** 
     * Die Methode prüft , ob die eingegebene Date(Input) korrekt ist.
     * 
     * @param {$} $inputElem 
     */
    var validiereEingabe = function ($inputElem) {
        var $inputName = $inputElem.attr('name');
        var $inputVal = $.trim($inputElem.val());
        switch ($inputName) {
            case "nachname":
            case "vorname":
                validiereName($inputVal) ? versteckeFehlermeldung($inputElem) : zeigeFehlermeldung($inputElem);
                return validiereName($inputVal);
                break;
            case "email":
                validiereEmail($inputVal) ? versteckeFehlermeldung($inputElem) : zeigeFehlermeldung($inputElem);
                return validiereEmail($inputVal);
                break;
            case "matrikelNummer" :
            case "adresse" :
                if ($inputVal != "") {
                    versteckeFehlermeldung($inputElem);
                } else {
                    $inputElem.closest(".inputFelder").removeClass("erfolgInput");
                    $inputElem.closest(".inputFelder").find("span").removeClass("glyphicon-ok");
                }
                return true;
                break;
            default:
        }
    };

    /** 
     * Die Methode zeigt ein Fehler-Icon an und blendy ein Erfolg-Icon icon aus.
     * 
     * @param {$} $inputElem 
     */
    var zeigeFehlermeldung = function ($inputElem) {
        $inputElem.closest(".inputFelder").removeClass("erfolgInput").addClass("errorInput");
        $inputElem.closest(".inputFelder").find("span").removeClass("glyphicon-ok").addClass("glyphicon-remove");
        $(".formularHeader .active span").addClass("errorTab");

    };

    /** 
     * Die Methode zeigt ein Erfolg-Icon an und blendy ein Fehler-Icon icon aus.
     * 
     * @param {$} $inputElem 
     */
    var versteckeFehlermeldung = function ($inputElem) {
        $inputElem.closest(".inputFelder").removeClass("errorInput").addClass("erfolgInput");
        $inputElem.closest(".inputFelder").find("span").removeClass("glyphicon-remove").addClass("glyphicon-ok");
    };

    /**
     * 
     * @param  {string} name
     */
    var validiereName = function (name) {
        var strReg = "^([ \u00c0-\u01ffa-zA-Z\.' \-]{2,})+$";
        var regex = new RegExp(strReg);
        return regex.test(name);
    }

    /**
     * 
     * @param  {string} email
     */
    var validiereEmail = function (email) {
        var strReg = "^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$";
        var regex = new RegExp(strReg);
        return regex.test(email);
    };

    /**
     * 
     */
    this.DOMUpdate = function () {
        $("#step1 input").each(function () {
            var name = $(this).attr("name");
            name = name.charAt(0).toUpperCase() + name.slice(1);
            $(this).val(student["get" + name]())
            validiereEingabe($(this));
        });
        tabsProgress();
        validiereKompletterStep();
    }
}





