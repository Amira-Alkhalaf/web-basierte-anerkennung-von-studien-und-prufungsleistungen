
function Step5(student, step3, step4) {
    var that = this
    var student = student;
    var step3 = step3;
    var step4 = step4;
    var studiengeange;

    this.init = function () {
        getStudiengange(function () {
            createModulTabelle();
        });
        zeigeStudentinfos(student.getVorname(), student.getNachname(), student.getAdresse(), student.getEmail())

    };

    this.addListeners = function () {

        $("#finalTabelle td > a.addChangeButton").click(addChange);

        // Edit row on edit button click
        $("#finalTabelle td > a.editButton").click(function () {
            if ($("#finalTabelle tr.active").length > 0) {
                var rowId = $(this).closest("tr").attr("id")
                $("#finalTabelle tr.active td > a.addChangeButton").click()
                editEingegebeneModul($('#' + rowId).find("td > a.editButton"));
            } else {
                editEingegebeneModul($(this));
            }
        });

        // Delete row on delete button click
        $("#finalTabelle td > a.removeButton").click(function () {
            toggleModul($(this));
        });


        $("#finalTabelle td > a.korrektButton").click(function () {
            toggleAkzeptiertesModul($(this))
        });

        $("#deleteTabelle  a.returnButton").click(function () {
            if ($("#deleteTabelle tr").length <= 1) {
                $('#titleNichtAkzeptierteModule').removeClass("active");
            }
            pushback($(this));
        });
        $("#finalTabelle tr").off("click").click(function (evt) {
            var $nichtAkzeptButton = $(this).find("a.removeButton");
            var $AkzeptButton = $(this).find("a.korrektButton");
            if ($(this).hasClass("nichtAkzeptiertesModul")) {
                nichtAkzeptiertesModul($nichtAkzeptButton)
                if (evt.ctrlKey) toggleModul($(this))
            } else if ($(this).hasClass("akzeptiertesModul")) {
                toggleAkzeptiertesModul($AkzeptButton)
                if (evt.ctrlKey) toggleModul($(this))
            }
        });
    }


    var createModulTabelle = function () {
        student.setLpSumme(0);
        student.setAkzeptierteLpSumme(0);
        //sumLP = 0;
        //neueSumme = 0;
        var parent = $('#finalTabelle tbody');
        var count = 0;
        var nameEingegebeneModul;
        var noteEingegebeneModul;
        var noteEingegebeneModul2;
        var anzahlEingegebeneVesuche;
        var eingegebenModuleIDs;
        parent.empty();
        if (student.getZuordnungen() == undefined) return;
        var AnzahlZuordnungen = student.getZuordnungen().length;
        var Zuordnungen = student.getZuordnungen();
        var belegteModule = student.getBelegteModule();
        for (var i = 0; i < AnzahlZuordnungen; i++) {
            eingegebenModuleIDs = Zuordnungen[i].benutzerModule;
            var anteile = Zuordnungen[i].anteile;
            var vorgeschlageneUniModul = Zuordnungen[i].uniModul.name;
            student.setLpSumme(student.getLpSumme() + parseInt(Zuordnungen[i].uniModul.lp));
            for (var k = 0; k < eingegebenModuleIDs.length; k++) {
                eingegebenModulID = eingegebenModuleIDs[k]
                var anteil = anteile[k];
                var $lastTr = $('<tr>').attr('id', 'finalTabelleRowId' + count);
                $lastTr.attr("data-zuordnungsindex", i);
                $lastTr.attr("data-nameUniModul", vorgeschlageneUniModul.toLowerCase().replace(/\s/g, ''));
                $lastTr.attr("data-belegtesmodulId", eingegebenModulID);
                var table = $("#finalTabelle").append($lastTr);
                var cols = $("#finalTabelle").find("tr:first th");
                for (var n = 0; n < belegteModule.length; n++) {
                    if (belegteModule[n].id === eingegebenModulID) {
                        if (Zuordnungen[i].status[k] === true) {
                            addiereLP(belegteModule[n].lp, anteil)
                        }
                        nameEingegebeneModul = belegteModule[n].modul;
                        lpEingegebeneModul = belegteModule[n].lp;
                        noteEingegebeneModul = belegteModule[n].getUmgerechneteNote();
                        anzahlEingegebeneVesuche = belegteModule[n].anzahlVersuche;
                        noteEingegebeneModul2 = belegteModule[n].note;
                    }
                }
                for (var j = 1; j <= cols.length; j++) {
                    switch (j) {
                        case 1:
                            $lastTr.append($('<td>').attr("name", "modulname").text(nameEingegebeneModul));
                            break;
                        case 2:
                            $lastTr.append($('<td>').attr("name", "modulLp").text(lpEingegebeneModul));
                            break;
                        case 3:
                            var isEng = getSelectedSprache() === 0;
                            $lastTr.append($('<td>').attr("name", "unimodul").addClass("uniModule")
                                .attr("data-switch-lang", isEng ? vorgeschlageneUniModul : studiengeange["EN"][vorgeschlageneUniModul])
                                .text(isEng ? studiengeange["EN"][vorgeschlageneUniModul] : vorgeschlageneUniModul));
                            break;
                        case 4:
                            $lastTr.append($('<td>').attr("name", "uniModulLp").text(Zuordnungen[i].uniModul.lp));
                            break;
                        case 5:
                            $lastTr.append($('<td>').attr("name", "anteil").addClass("prozent").text(anteil));
                            break;
                        case 6:
                            $lastTr.append($('<td>').attr("name", "note").text(noteEingegebeneModul2));
                            break;
                        case 7:
                            $lastTr.append($('<td>').attr("name", "note2").addClass("uninote").text(noteEingegebeneModul));
                            break;
                        case 8:
                            $lastTr.append($('<td>').attr("name", "anzahlversuche").text(anzahlEingegebeneVesuche));
                            break;
                        case 9:
                            $lastTr.append($('<td>').addClass("Zustimmung"));
                            var $lastTD = $("#finalTabelle td").last();
                            $lastTD.append($('<a/>').addClass("btn btn-xs addChangeButton").attr("title", "add change")
                                .append($('<i/>').addClass("glyphicon glyphicon-refresh")))
                                .append($('<a/>').addClass("btn btn-xs editButton").attr("title", "edit")
                                    .append($('<i/>').addClass("glyphicon glyphicon-pencil")))
                                .append($('<a/>').addClass("btn btn-xs removeButton").attr("title", "remove")
                                    .append($('<i/>').addClass("glyphicon glyphicon-remove")))
                                .append($('<a/>').addClass("btn btn-xs korrektButton")
                                    .append($('<i/>').addClass("glyphicon glyphicon-ok")));
                            if (Zuordnungen[i].status[k]) {
                                toggleAkzeptiertesModul($lastTr.find("a.korrektButton"))
                            } else if (Zuordnungen[i].status[k] === false) {
                                nichtAkzeptiertesModul($lastTr.find("a.removeButton"))
                            }
                            break;
                        default:
                            break;
                    }
                }
                count++;
            }
        }
        validiereAusgewaehlteProzent()
        zeigeSumLP(student.getLpSumme(), student.getAkzeptierteLpSumme());

        $("#finalTabelle td > a.addChangeButton").click(addChange);

        // Edit row on edit button click
        $("#finalTabelle td > a.editButton").click(function () {
            if ($("#finalTabelle tr.active").length > 0) {
                var rowId = $(this).closest("tr").attr("id")
                $("#finalTabelle tr.active td > a.addChangeButton").click()
                editEingegebeneModul($('#' + rowId).find("td > a.editButton"));
            } else {
                editEingegebeneModul($(this));
            }
        });


        // Delete row on delete button click
        $("#finalTabelle td > a.removeButton").click(function () {
            nichtAkzeptiertesModul($(this))
            toggleModul($(this))
        });

        $("#finalTabelle td > a.korrektButton").click(function () {
            toggleAkzeptiertesModul($(this))
            toggleModul($(this))
        });

        $("#finalTabelle tr").off("click").click(function (evt) {

            if (evt.ctrlKey) {
                var $nichtAkzeptButton = $(this).find("a.removeButton");
                var $AkzeptButton = $(this).find("a.korrektButton");
                if ($(this).hasClass("nichtAkzeptiertesModul")) {
                    nichtAkzeptiertesModul($nichtAkzeptButton)
                    toggleModul($(this))
                } else if ($(this).hasClass("akzeptiertesModul")) {
                    toggleAkzeptiertesModul($AkzeptButton)
                    toggleModul($(this))
                }
            }
        });



    };



    var validiereAusgewaehlteProzent = function () {
        $("#finalTabelle tr").slice(1).each(function () {
            var $this = $(this);
            var ModulId = $this.attr("data-belegtesmodulid");
            var nameUniModul = $this.attr("data-nameUniModul");
            var $moduleRows = $("#finalTabelle").find("[data-belegtesmodulid=" + ModulId + "]")
            var $uniModuleRows = $("#finalTabelle").find("[data-nameUniModul=" + nameUniModul + "]")
            var moduleNum = $moduleRows.length;
            var uniModuleNum = $uniModuleRows.length;

            if (moduleNum > 1 || uniModuleNum > 1) {
                if (moduleNum > 1) {
                    var sum = 0
                    $moduleRows.find("td[name='anteil']").each(function () {
                        if (!isNaN(parseInt($(this).text()))) {
                            sum += parseInt($(this).text())
                        }
                    });
                    if (sum > 100) {
                        $(this).find("td[name='anteil']").addClass("error");
                        $(this).find("td[name='modulname']").addClass("error");
                    }
                }
                // if (uniModuleNum > 1 && nameUniModul != "wählbaremodulelautmodulhandbuch") {
                //     var sum = 0
                //     $uniModuleRows.find("td[name='anteil']").each(function () {
                //         if (!isNaN(parseInt($(this).text()))) {
                //             sum += parseInt($(this).text())
                //         }
                //     });
                //     if (sum > 100) {
                //         $this.find("td[name='anteil']").addClass("error");
                //         $this.find("td[name='unimodul']").addClass("error")
                //     }
                // }
            }
        });
    }

    var addChange = function () {

        var $this = $(this)
        var empty = false;
        var input = $this.parents("tr").find('input[type="text"], input[type="number"]');
        var select = $this.parents("tr").find('select option:selected');
        var name = $this.attr("name");
        var zuordnung_index = $(input[0]).closest("tr").data("zuordnungsindex");

        zuordnung_index = parseInt(zuordnung_index);
        zuordnung = student.getZuordnungen()[zuordnung_index];
        modulid = $(input[0]).closest("tr").data("belegtesmodulid");
        modulid = parseInt(modulid);
        modulindex = zuordnung.getBenutzerModul(modulid);

        input.add(select).each(function () {
            switch ($(this).closest("td").attr("name")) {
                case "modulname":
                    if (!step3.validateEingegebenesModul($(this).val())) {
                        $(this).addClass("error");
                        empty = true;
                    } else {
                        $(this).removeClass("error");
                        student.getBelegtesModul(modulid).modul = $(this).val();
                    }
                    break;
                case "modulLp":
                    if (parseInt($(this).val()) != 0 && $(this).val() != "") {
                        student.getBelegtesModul(modulid).lp = $(this).val();
                        $(this).removeClass("error");
                    } else {
                        $(this).addClass("error")
                        empty = true;
                    }
                    break;
                case "unimodul":
                    isEng = getSelectedSprache() === 0;
                    zuordnung.uniModul.name = isEng ? $(this).attr("data-switch-lang") : $(this).val();
                    zuordnung.uniModul.bereich = $(this).attr("data-bereich");
                    zuordnung.uniModul.lp = $(this).attr("data-lp");
                    break;
                case "anteil":
                    zuordnung.anteile[modulindex] = $(this).val();
                    // }
                    break;
                case "note":
                    if (!step3.validateEingegebeneNote($(this), $(this).val())) {
                        $(this).addClass("error");
                        empty = true;
                    } else {
                        $(this).removeClass("error");
                        // student.getBelegtesModul(modulid).note = parseFloat($(this).val());
                        student.getBelegtesModul(modulid).note = $(this).val();
                        student.getBelegtesModul(modulid).setUmgerechneteNote(student.getNotenSystem());
                    }
                    break;
                case "anzahlversuche":
                    if ($(this).val() && parseInt($(this).val()) > 3) {
                        $(this).addClass("error");
                        empty = true;
                    }
                    else {
                        if (!isNaN(parseInt($(this).val()))) {
                            student.getBelegtesModul(modulid).anzahlVersuche = parseInt($(this).val());
                        }
                        $(this).removeClass("error");
                    }
            }
        });
        $this.parents("tr").find(".error").first().focus();
        if (!empty) {
            input.each(function () {
                $(this).parent("td").html($(this).val());
            });
            select.each(function () {
                $(this).parents("td").html($(this).text());
            });

            $this.parents("tr").find(".addChangeButton, .editButton, .removeButton, .korrektButton").toggle();
            $(".add-new").removeAttr("disabled");
        }
        createModulTabelle();
        // setze umgerechnete note wieder
        $this.parents("tr").find("td[name='note2']").text(student.getBelegtesModul(modulid).getUmgerechneteNote());

        // update step 3 und step 4 
        step3.DOMUpdate();
        step4.init(step4.DOMUpdate);

    };

    var editEingegebeneModul = function ($this) {

        var $tds = $this.parents("tr").find("td:not(.Zustimmung)")
        $this.parents("tr").addClass("active");
        unimodul_text = $this.parents("tr").find("td[name='unimodul']").text();
        $tds.each(function () {
            if ($(this).hasClass("uniModule")) {
                setUniModule($(this), unimodul_text);
            } else if ($(this).attr("name") === "uniModulLp") {
                $(this).html('<input  type="text" class="form-control" value="' + $(this).text() + '" readonly>');
            } else if ($(this).hasClass("prozent")) {
                currrent_selected = $(this).text();
                $select = $("<select>").addClass("form-control");
                for (var i = 0; i < 4; i++) {
                    if ((i + 1) * 25 === parseInt(currrent_selected)) {
                        option_html = '<option selected>' + currrent_selected + '</option>'
                    }
                    else {
                        option_html = '<option>' + (i + 1) * 25 + '</option>'
                    }
                    $select.append(option_html);
                }
                $(this).html($select);
            }
            else if ($(this).attr("name") === "note2") {
                $(this).html('<input  type="text" class="form-control" value="' + $(this).text() + '" readonly>');

            }
            else if ($(this).attr("name") === "anzahlVersuche") {
                $(this).html('<input type="number" data-placement="top" data-toggle="VersuchePopover" min="0" max="3" class="form-control" value="' + $(this).text() + '">');
            }
            else if ($(this).attr("name") === "modulLp") {
                $(this).html('<input type="number" class="form-control" value="' + $(this).text() + '">');
            }
            else {
                $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
            }
        });
        $this.parents("tr").find(".addChangeButton, .editButton, .removeButton, .korrektButton").toggle();
    };


    var toggleAkzeptiertesModul = function ($this) {

        $this.closest('tr').find(".editButton, .removeButton").toggle();
        $this.closest('tr').toggleClass('akzeptiertesModul');
    };


    var nichtAkzeptiertesModul = function ($this) {
        $this.closest('tr').find(".editButton, .korrektButton").toggle();
        $this.closest('tr').toggleClass('nichtAkzeptiertesModul');
    };

    var toggleModul = function ($this) {

        var $selectedRow = $this.closest('tr');
        var lp = $selectedRow.find("td[name='modulLp']").text();
        var anteil = $selectedRow.find("td[name='anteil']").text()
        var zuordnungsindex = $selectedRow.data("zuordnungsindex");
        zuordnungsindex = parseInt(zuordnungsindex);
        var modulid = $selectedRow.data("belegtesmodulid");
        modulid = parseInt(modulid);
        var zuordnungen = student.getZuordnungen();
        var modulindex = zuordnungen[zuordnungsindex].getBenutzerModul(modulid);
        if ($this.closest('tr').hasClass("akzeptiertesModul")) {
            zuordnungen[zuordnungsindex].status[modulindex] = true;
            addiereLP(lp, anteil)
        } else if ($this.closest('tr').hasClass("nichtAkzeptiertesModul")) {
            zuordnungen[zuordnungsindex].status[modulindex] = false;
        } else {
            if (zuordnungen[zuordnungsindex].status[modulindex]) {
                subtrahiereLP(lp, anteil)
            }
            zuordnungen[zuordnungsindex].status[modulindex] = "";
        }
        $("#summe .neueSummeLp").text(student.getAkzeptierteLpSumme())
    };

    var setUniModule = function ($this, selected_unimodul) {
        var studiengang = studiengeange["DE"][student.getStudiengang()];
        var test = $this.html('<select class="form-control unimodule" ></select>');
        $.each(studiengang, function (key, value) {
            for (var i = 0; i < value.length; i++) {
                var deutschesModulName = value[i].name.toLowerCase().replace(/\s/g, '')
                var istDuplikat = $this.find('select.unimodule option[name=' + deutschesModulName + ']').length > 0
                //   if (!istDuplikat) {
                if (getSelectedSprache() === 1) {
                    if (value[i].name === selected_unimodul) {
                        option_html = $("<option>").attr({
                            'data-bereich': key,
                            'data-switch-lang': studiengeange["EN"][value[i].name],
                            'data-lp': value[i].lp,
                            'name': value[i].name.toLowerCase().replace(/\s/g, ''),
                            selected: 'selected'
                        }).text(value[i].name);
                    } else {
                        option_html = $("<option>").attr({
                            'data-bereich': key,
                            'data-lp': value[i].lp,
                            'data-switch-lang': studiengeange["EN"][value[i].name],
                            'name': value[i].name.toLowerCase().replace(/\s/g, ''),
                        }).text(value[i].name);
                    }
                } else {
                    if (studiengeange["EN"][value[i].name] === selected_unimodul) {
                        option_html = $("<option>").attr({
                            'data-bereich': key,
                            'data-lp': value[i].lp,
                            'data-switch-lang': value[i].name,
                            'name': value[i].name.toLowerCase().replace(/\s/g, ''),
                            selected: 'selected'
                        }).text(studiengeange["EN"][value[i].name]);
                    } else {
                        option_html = $("<option>").attr({
                            'data-bereich': key,
                            'data-lp': value[i].lp,
                            'data-switch-lang': value[i].name,
                            'name': value[i].name.toLowerCase().replace(/\s/g, ''),
                        }).text(studiengeange["EN"][value[i].name]);
                    }
                }
                $this.parents("tr").find("select.unimodule").append(option_html);
            }
            //    }
        });
    }


    var addiereLP = function (lp, anteil) {

        var umgerechneterLp = Math.round(parseInt(lp) * parseInt(anteil) / 100.0);
        if (!isNaN(parseInt(anteil))) {
            student.setAkzeptierteLpSumme(student.getAkzeptierteLpSumme() + umgerechneterLp);
        } else {
            student.setAkzeptierteLpSumme(student.getAkzeptierteLpSumme() + parseInt(lp));
        }
    }
    var subtrahiereLP = function (lp, anteil) {
        var umgerechneterLp = Math.round(parseInt(lp) * parseInt(anteil) / 100.0);
        if (!isNaN(parseInt(anteil))) {
            student.setAkzeptierteLpSumme(student.getAkzeptierteLpSumme() - umgerechneterLp);
        } else {
            student.setAkzeptierteLpSumme(student.getAkzeptierteLpSumme() - parseInt(lp));
        }
    }

    var getStudiengange = function (callback) {
        $.getJSON("data/studiengaenge.json", function (data) {
            studiengeange = data;
            callback(studiengeange);
        });
    }

}














