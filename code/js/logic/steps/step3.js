
function Step3(student) {
    var that = this;
    var student = student;
    var belegteModule;
    var stepNr = 3;
    var aktuellNotenSystem;
    var step4;

    this.init = function () {
        this.addListeners();
        setHelpToolTip();
    };

    this.addListeners = function () {
        $("#step3 .next-step").off("click").click(function () {
            belegteModule = [];
            if (validiereTabelle()) {
                versteckeFehlermeldung()
                student.setBelegteModule(belegteModule);
                step4 = new Step4(student, that);
                step4.init(step4.DOMUpdate);
                oeffneNaechsterStep();
            } else {
                zeigeFehlermeldung();
            }
        });
        $("#step3 input").off("keydown").keydown(function () {
            schliesseNaechsteSteps(stepNr);
        });
        $("#AddRowButton").off('click').click(function () {
            schliesseNaechsteSteps(stepNr);
            getAddierteZeile();
        });
        $("#step2 .next-step").unbind('click', initialisiereVorgegebeZeile).on("click", initialisiereVorgegebeZeile)


        $("#step3 select").editableSelect({ filter: false, duration: 50 }).on('select.editable-select', function (e, li) {
            schliesseNaechsteSteps(stepNr);
        });
    };

    var setHelpToolTip = function () {
        $("#step3 a.infos").tooltip({
            'selector': '',
            'placement': 'right',
            'html': 'true',
            'title': "<b data-lang='noteTitel'>Note:</b> <span> Note im Notensystem des Herkunftlandes.</span><br>"
                + "<b data-lang='lpTitel'>ECTS-Punkte:</b> Ein ECTS Punkt entspricht 25 bis 30 Arbeitsstunden. Im Vollzeitstudium werden ca. 60 Leistungspunkte pro akademischem Jahr erreicht.<br>"
                + "<b data-lang='versucheTitel'>Fehlversuche:</b> Wie oft wurde die Prüfung des Moduls nicht bestanden."
        });
    }

    this.updateTooltip = function () {
        if (getSelectedSprache() === 1) {
            $("#step3 a.infos").tooltip('hide')
                .attr('data-original-title', "<b data-lang='noteTitel'>Note:</b> <span> Note im Notensystem des Herkunftlandes.</span><br>"
                    + "<b data-lang='lpTitel'>ECTS-Punkte:</b> Ein ECTS Punkt entspricht 25 bis 30 Arbeitsstunden. Im Vollzeitstudium werden ca. 60 ECTS Punkte pro akademischem Jahr erreicht.<br>"
                    + "<b data-lang='versucheTitel'>Fehlversuche:</b> Wie oft wurde die Prüfung des Moduls nicht bestanden.")
                .tooltip('fixTitle')

        } else {
            $("#step3 a.infos").tooltip('hide')
                .attr('data-original-title', "<b data-lang='noteTitel'>Grade:</b> <span> Grade in the grading system of the country of origin.</span><br>"
                    + "<b data-lang='lpTitel'>ECTS points:</b> One ECTS point is equivalent to 25 to 30 working hours. Studying full-time achieves about 60 ECTS points per academic year.<br>"
                    + "<b data-lang='versucheTitel'>Failed attempts:</b> How many times the exam for the modul was failed.")
                .tooltip('fixTitle')

        }
    }

    var zeigeFehlermeldung = function () {
        $(".formularHeader .active span").removeClass("erfolgTab").addClass('errorTab');
        schliesseNaechsteSteps(stepNr);
    };

    var versteckeFehlermeldung = function () {
        $(".formularHeader .active span").removeClass("errorTab").addClass("erfolgTab");
    };


    var initialisiereVorgegebeZeile = function () {
        setNotenSelect();
        if ($("#moduleTabelle tbody tr").length == 1) {
            // false for not calling addListeners
            getAddierteZeile(false);
        }
    }

    /**
     * falls das Notensystem in step2 geändert 
     * ändern gegebenfalls diese Funtion .
     */
    var setNotenSelect = function () {
        if (student.getNotenSystem() === aktuellNotenSystem) return;
        aktuellNotenSystem = student.getNotenSystem();
        $("#step3 input.es-input").each(function () {
            $parent = $(this).closest("td.inputTabelle");
            $(this).remove();
            $("ul", $parent)[0].remove();
            $select = $('<select>').attr({
                "name": $(this).attr("name"),
                "data-toggle": "NotePopover",
                "class": "form-control",
                "data-trigger": "focus",
                "data-placement": "top"
            });
            $parent.append($select);
            addiereOptionen($select);
        });

        $("#step3 select").each(function () {
            addiereOptionen($(this));
        });

    }
    //TODO: nicht benutze funktion
    this.updateUser = function () {
    }
    //TODO: nicht benutze funktion
    this.getStudent = function () {
        return student;
    }


    var getAddierteZeile = function (callListeners = true) {
        $row = addiereZeile()
        $('a.deleteRow').off("click").click(function () {
            schliesseNaechsteSteps(stepNr);
            loescheAusgewaehlteZiele($(this));
        });
        if (callListeners) that.addListeners();
        return $row;
    }


    var parseUserModuls = function (modul, note, lp, anzahlVersuche, id) {
        var belegteModul = new BelegtesModul(modul, note, lp, anzahlVersuche);
        belegteModul.setUmgerechneteNote(student.getNotenSystem());
        belegteModul.init(id);
        belegteModule.push(belegteModul);

    }

    var loescheAusgewaehlteZiele = function ($this) {
        var rowNum = $('#moduleTabelle tr').size();
        if (rowNum > 2) {
            $this.closest('tr').remove();
            student.updateZuordnungen(parseInt($this.closest('tr').attr("data-modulid")))
        }
    }


    var validiereTabelle = function () {
        var modul;
        var Note;
        var LP;
        var AnzahlVersuche;
        var $inputRow;
        var istVersucheState;
        var istvalidate = true;
        $("#moduleTabelle tr").slice(1).each(function () {
            modul = "";
            Note = "";
            LP = "";
            AnzahlVersuche = "";
            $inputRow = $(this);
            istVersucheState = true;
            id = parseInt($inputRow.attr("data-modulid"))
            $inputRow.find("input, select ").each(function () {
                var $inputElm = $(this);
                var $inputName = $inputElm.attr('name');
                var $inputValue = $inputElm.val().trim();
                switch ($inputName) {
                    case "modul":
                        modul = $inputValue;
                        that.validateEingegebenesModul(modul) ? $inputElm.removeClass("has-error") : $inputElm.addClass("has-error");
                        break;
                    case "note":
                        if (that.validateEingegebeneNote($inputElm, $inputValue)) {
                            $inputElm.removeClass("has-error");
                            Note = $inputValue;
                        } else {
                            $inputElm.addClass("has-error");
                            Note = "";
                        }
                        break;
                    case "lp":
                        if (parseInt($inputValue) != 0 && $inputValue != "" ) {
                            $inputElm.removeClass("has-error");
                            LP = $inputValue;
                        } else {
                            $inputElm.addClass("has-error");
                            LP = "";
                        }
                        break;
                    case "anzahlVersuche":
                        AnzahlVersuche = $inputValue;
                        istVersucheState = that.validateEingegebeneVersuche(AnzahlVersuche, $inputElm);
                        istVersucheState ? $inputElm.removeClass("has-error") : $inputElm.addClass("has-error");
                        break;
                    default:
                        break;
                }
            });
            if (Note && modul && istVersucheState && LP) {
                $inputRow.removeClass("errorInput").addClass("erfolgInput");
                parseUserModuls(modul, Note, LP, AnzahlVersuche, id);
            } else {
                $inputRow.removeClass("erfolgInput").addClass("errorInput");
                istvalidate = false
                return false
            }
        });
        return istvalidate;
    };

    this.validateEingegebenesModul = function (name) {
        return name.length > 0;
    }


    this.validateEingegebeneVersuche = function (anzahlVersuche, $this) {
        var maxVersucheAnzahl = parseInt($this.attr('max'));
        var minVersucheAnzahl = parseInt($this.attr('min'));
        if (anzahlVersuche >= minVersucheAnzahl && anzahlVersuche <= maxVersucheAnzahl) {
            return true;
        } else {
            $this.attr("data-content", "MIN= " + minVersucheAnzahl + ", " + "MAX= " + maxVersucheAnzahl).popover('show');

            setTimeout(function () {
                $this.popover('hide');
            }, 1000);
            return false
        }
    };


    this.validateEingegebeneNote = function ($inputElm, inputValue) {

        var istNoteState = false;
        setMinMaxNote()
        if (isNaN(parseFloat(minNote))) {
            noten = student.getNotenSystem();
            if (noten[inputValue.toUpperCase()] !== undefined) {
                istNoteState = true;
                $inputElm ? $inputElm.popover('hide') : null;
            } else {
                istNoteState = false;
                $inputElm ? $inputElm.attr("data-content", "MIN= " + minNote + " , " + "MAX= " + maxNote).popover('show') : null;
            }
        }
        else {
            minNote = parseFloat(minNote);
            maxNote = parseFloat(maxNote);
            inputValue = parseFloat(inputValue)
            if (maxNote - minNote > 0) {
                if (inputValue >= minNote && inputValue <= maxNote) {
                    istNoteState = true;
                    $inputElm ? $inputElm.popover('hide') : null;
                } else {
                    $inputElm ? $inputElm.attr("data-content", "MIN= " + minNote + " , " + "MAX= " + maxNote).popover('show') : null;
                    setTimeout(function () {
                        $inputElm ? $inputElm.popover('hide') : null;
                    }, 1000);
                    istNoteState = false
                }
            } else {
                if (inputValue >= maxNote && inputValue <= minNote) {
                    istNoteState = true;
                    $inputElm ? $inputElm.popover('hide') : null;
                } else {
                    $inputElm ? $inputElm.attr("data-content", "MIN= " + minNote + " , " + "MAX= " + maxNote).popover('show') : null;
                    setTimeout(function () {
                        $inputElm ? $inputElm.popover('hide') : null;
                    }, 1000);
                    istNoteState = false
                }
            }
        }

        return istNoteState;
    };

    var setMinMaxNote = function () {
        var notenSystem = student.getNotenSystem();
        var keys = Object.keys(student.getNotenSystem());
        var vals = Object.values(notenSystem);

        var arr = [];
        for (var key in notenSystem) {
            if (notenSystem.hasOwnProperty(key)) {
                arr.push({
                    'key': key,
                    'value': notenSystem[key]
                });
            }
        }
        arr = arr.sort(function (a, b) { return a.value - b.value; });
        minNote = arr[0].key;
        maxNote = arr[(arr.length) - 1].key;
    };


    this.DOMUpdate = function () {
        belegteModule = student.getBelegteModule();
        for (var i = 0; i < belegteModule.length; i++) {
            $tr = $('#moduleTabelle tr[data-modulid="' + belegteModule[i].id + '"]');
            if ($tr.length === 0) {
                $tr = getAddierteZeile();
                $tr.attr("data-modulid", belegteModule[i].id)
            }
            $tr.find("td input[data-name=modulname]").val(belegteModule[i].modul);
            $tr.find("td input[data-name=versuche]").val(belegteModule[i].anzahlVersuche);
            $tr.find("td input[data-name=note]").val(belegteModule[i].note);
            $tr.find("td input[data-name=lp]").val(belegteModule[i].lp);
        }

    }

    this.deleteRows = function () {
        $("#moduleTabelle tr").slice(1).remove();
    }

    var addiereZeile = function () {
        var $lastTr = $('<tr>');
        $lastTr.attr("data-modulid", Math.floor(Math.random() * 100000000) + 1);
        var table = $("#moduleTabelle").append($lastTr);
        var cols = $("#moduleTabelle").find("tr:first th");

        for (var i = 1; i <= cols.length; i++) {
            var input = $lastTr.append($('<td>').addClass("inputTabelle"));
            var $lastTD = $("#moduleTabelle .inputTabelle").last();
            switch (i) {
                case 1:
                    $('<input type="text" autofocus>').attr("name", "modul").attr("data-name", "modulname").appendTo($lastTD);
                    break;
                case 2:
                    $select = $('<select>').attr({
                        "name": "note",
                        "data-name": "note",
                        "data-toggle": "NotePopover",
                        "class": "form-control",
                        "data-trigger": "focus",
                        "data-placement": "top"
                    });
                    $select.appendTo($lastTD);
                    addiereOptionen($select);

                    break;
                case 3:
                    $('<input type="number">').attr("name", "lp").attr("data-name", "lp").appendTo($lastTD);
                    break;
                case 4:
                    $('<input type="number" >').attr({
                        "name": "anzahlVersuche",
                        "data-name": "versuche",
                        "max": 3,
                        "min": 0,
                        "data-toggle": "VersuchePopover",
                        "data-placement": "top"
                    }).appendTo($lastTD);
                    break;
                case 5:
                    $lastTD.append(($('<a/>').addClass("btn btn-xs deleteRow")
                        .append($('<i/>').addClass("glyphicon glyphicon-trash"))));
                    break;
                default:
                    break;
            }
        }
        return $lastTr;
    }


    var addiereOptionen = function ($select) {
        var selectedNotenSystem = student.getNotenSystem();
        var keys = [];
        var values = [];
        try {
            for (var k in selectedNotenSystem) {
                if (isNaN(parseFloat(k))) throw "parseException";
                keys.push(parseFloat(k));
            }
            keys.sort(function (a, b) { return a - b; });
        }
        catch (e) {
            for (var k in selectedNotenSystem) values.push(parseFloat(selectedNotenSystem[k]));
            values.sort(function (a, b) { return a - b; });
        }
        $select.children('option').remove();
        var option = new Option();
        $select.append($(option));
        if (keys.length !== 0) {
            for (var i in keys) {
                var option = new Option(keys[i], keys[i], false, false);
                $select.append($(option));
            }
        } else {
            already_used_keys = []
            for (var v in values) {
                for (var k in selectedNotenSystem) {
                    if (!already_used_keys.includes(k) && parseFloat(selectedNotenSystem[k]) === values[v]) {
                        var option = new Option(k, k, false, false);
                        $select.append($(option));
                        already_used_keys.push(k);
                    }
                }
            }
        }


        that.addListeners();

    }

    this.getStep4 = function () {
        if (step4 === undefined) {
            return new Step4(student, this)
        }
        return step4
    }

    this.setCurrentNotenSystem = function (_aktuellNotenSystem) {
        aktuellNotenSystem = _aktuellNotenSystem;
    }

}
