
function Step2(student) {
    var that = this;
    var student = student;
    var aktuellesLand;
    var alterStudiengang;
    var studiengang;
    var studiengangMap;
    // Noten map für jedes Land
    var landNotenMap;
    // Verfügbaren Notensysteme für das aktuelle Land 
    var vefuegbareNotenSysteme;
    var stepNr = 2;

    this.init = function () {
        baueLaender();
        baueStudiengaenge();
        this.addListeners();
    };

    this.getLand = function () {
        return land;
    };
    this.getAlterStudiengang = function () {
        return alterStudiengang;
    };
    this.getStudiengang = function () {
        return studiengang;
    };

    this.getStudent = function () {
        return student;
    }

    this.addListeners = function () {

        $("#step2 select").off("change").on("change", function () {
            $element = $(this);
            var selectedVal = $element.find(":selected").val();
            that.updateUser($element, selectedVal);
            schliesseNaechsteSteps(stepNr);
            versteckeFehlermeldung($element);
            if ($element.is("#land")) {
                that.removeAlteNoten();
                baueNotenOptionen(landNotenMap);
            }
            if ($element.is("#notenSystem")) {
                var index = $('#notenSystem').prop('selectedIndex');
                var ausgewaehltNotenSystem = vefuegbareNotenSysteme[index - 1];
                student.setNotenSystem(ausgewaehltNotenSystem);
            }
        });
        $("#step2 input").off("change").on("change", function () {
            $inputElem = $(this);
            schliesseNaechsteSteps(stepNr);
            if (!$inputElem.is("#einstufungFachsemester")) {
                versteckeFehlermeldung($inputElem);
                that.updateUser($inputElem,$inputElem.val());
            } else {
                selectedVal = $inputElem.is(':checked');
                that.updateUser($inputElem, selectedVal);
            }
        });

        $("#step2 .next-step").off("click").click(function () {
            if (validiereKompletterStep()) {
                oeffneNaechsterStep();
            }
        });
    };

    var baueLaender = function () {
        if (landNotenMap === undefined) {
            $.getJSON("data/noten.json", function (data) {
                landNotenMap = data;
                baueLaenderOptionen(landNotenMap);
            });
        } else {
            baueLaenderOptionen(landNotenMap);
        }
    };

    var baueLaenderOptionen = function (landNotenMap) {
        var parent = $("#land");
        var andereSprache = getSelectedSprache() === 1 ? 0 : 1;
        $.each(landNotenMap, function (key, val) {
          //  parent.append($("<option>").attr("value", val.Land[getSelectedSprache()]).attr("data-switch-lang", val.Land[andereSprache]).text(val.Land[getSelectedSprache()]));
            parent.append($("<option>").attr("value", val.Land[1]).attr("data-switch-lang", val.Land[andereSprache]).text(val.Land[getSelectedSprache()]));
        });
        sortiereOptionen(parent);
    }


    var baueStudiengaenge = function () {
        if (studiengangMap === undefined) {
            $.getJSON("data/studiengaenge.json", function (data) {
                studiengangMap = data;
                baueStudiengaengeOptionen(studiengangMap)

            });
        } else {
            baueStudiengaengeOptionen(studiengangMap)
        }
    };


    var baueStudiengaengeOptionen = function (studiengangMap) {
        parent = $("#studiengang");
        var sprache = getSelectedSprache() === 1 ? "DE" : "EN";
        $.each(studiengangMap["DE"], function (key, val) {
            var toDisplay = sprache === "DE" ? key : studiengangMap["EN"][key]
            var toswitch = sprache !== "DE" ? key : studiengangMap["EN"][key]
            parent.append($("<option>").attr("value", key).attr("data-switch-lang", toswitch).text(toDisplay));

        });
        sortiereOptionen(parent);
    }

    var sortiereOptionen = function (parent) {

        parent.append(parent.find("option").remove().sort(function (a, b) {
            var at = $(a).text(), bt = $(b).text();
            at = at.toLowerCase();
            at = at.replace(/ä/g, "a");
            at = at.replace(/ö/g, "o");
            at = at.replace(/ü/g, "u");
            at = at.replace(/ß/g, "s");

            bt = bt.toLowerCase();
            bt = bt.replace(/ä/g, "a");
            bt = bt.replace(/ö/g, "o");
            bt = bt.replace(/ü/g, "u");
            bt = bt.replace(/ß/g, "s");

            return (at == bt) ? 0 : (at > bt) ? 1 : -1;
        }));
        parent.val(parent.find("option:first").val());
    }

    var baueNotenOptionen = function (data) {
        $.each(data, function (key, val) {
            if (student.getLand() === val.Land[1]) {
                vefuegbareNotenSysteme = val.Noten;
                $.each(vefuegbareNotenSysteme, function (k, value) {
                    var str = getMinMaxNotenString(value);
                    $("<option>").val(str).text(str).appendTo("#notenSystem");
                })
            };
        });
    };

    var getMinMaxNotenString = function (notenSystem) {
        var keys = Object.keys(notenSystem);
        var vals = Object.values(notenSystem);

        var arr = [];
        for (var key in notenSystem) {
            if (notenSystem.hasOwnProperty(key)) {
                arr.push({
                    'key': key,
                    'value': notenSystem[key]
                });
            }
        }
        arr = arr.sort(function (a, b) { return a.value - b.value; });
        var min = arr[0].key;
        var max = arr[(arr.length) - 1].key;
        return "Min:" + min + ", " + "Max:" + max
    }

    var validiereKompletterStep = function () {
        if (student.getLand() && student.getStudiengang() && student.getNotenSystem()) {
            $(".formularHeader .active span").removeClass("errorTab").addClass("erfolgTab");
            return true;
        } else {
            $(".formularHeader .active span").addClass('errorTab');
            $(".formularWrapper #step2 .inputFelder").not(".erfolgInput").not(".optionalInput").addClass("errorInput");
            $(".formularWrapper #step2 .inputFelder > span").not(".glyphicon-ok").addClass("glyphicon-remove");
            return false;
        }
    };

    this.updateUser = function ($selectElem, selectedVal) {

        var name = $selectElem.attr("name");
        name = name.charAt(0).toUpperCase() + name.slice(1);
        student["set" + name](selectedVal);
    };

    var validiereAusgewaehltElem = function ($this, selectedVal) {
        if (selectedVal != "") {
            versteckeFehlermeldung($this)
            return true;
        } else {
            zeigeFehlermeldung($this)
            return false;
        }
    };
    var zeigeFehlermeldung = function ($inputElem) {
        $(".formularHeader .active span").addClass('errorTab');
        $inputElem.closest(".inputFelder").removeClass("erfolgInput").addClass("errorInput");
        $inputElem.closest(".inputFelder").find("span").removeClass("glyphicon-ok").addClass("glyphicon-remove");
        schliesseNaechsteSteps(stepNr);
    };

    var versteckeFehlermeldung = function ($inputElem) {
        $inputElem.closest(".inputFelder").removeClass("errorInput").addClass("erfolgInput");
        $inputElem.closest(".inputFelder").find("span").removeClass("glyphicon-remove").addClass("glyphicon-ok");
    };


    this.removeAlteNoten = function () {
        aktuellesLand !== student.getLand() ? student.setNotenSystem("") : null;
        var $selectOption = $("#notenSystem option");
        $selectOption.first().prop('selected', true);
        $selectOption.slice(1).remove();
        $selectOption.closest(".inputFelder").removeClass("erfolgInput")
        $selectOption.closest(".inputFelder").find("span").removeClass("glyphicon-ok")

    }

    this.removeAlteStudiengaenge = function () {
        student.setStudiengang("");
        var $selectedStudiengang = $("#studiengang option");
        $selectedStudiengang.slice(1).remove();
        $selectedStudiengang.closest(".inputFelder").removeClass("erfolgInput")
        $selectedStudiengang.closest(".inputFelder").find("span").removeClass("glyphicon-ok")
    }

    this.removeAltelaender = function () {
        student.setLand("");
        var $laender = $("#land option");
        $laender.slice(1).remove();
        $laender.closest(".inputFelder").removeClass("erfolgInput")
        $laender.closest(".inputFelder").find("span").removeClass("glyphicon-ok")
    }

    this.uebersetze = function () {

        var $laender = $("#land option");
        $laender.slice(1).remove();
        var elemt = $("#land").find(":selected").val();
        student.setLand(elemt);
    }

    this.DOMUpdate = function () {
        aktuellesLand = student.getLand();
        $("#step2 input, #step2 select").each(function () {
            var name = $(this).attr("name");
            name = name.charAt(0).toUpperCase() + name.slice(1);
            if (name === "Land") {
                $(this).val(student["get" + name]());
                baueNotenOptionen(landNotenMap);
            }if (name === "EinstufungFachsemester") {
                $(this).prop('checked', student["get" + name]());
            }if (name === "NotenSystem") {
                $(this).val(getMinMaxNotenString(student["get" + name]()));
            }else{
                $(this).val(student["get" + name]());
            }

            $(this).change();
            tabsProgress();
        });
    }

}

