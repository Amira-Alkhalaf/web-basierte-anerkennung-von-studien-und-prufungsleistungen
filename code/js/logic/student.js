

function Student() {

    var vorname;
    var nachname;
    var adresse;
    var matrikelNummer;
    var email;
    var land;
    var alterStudiengang;
    var studiengang;
    var notenSystem;
    var einstufungFachsemester = false;
    var belegteModule = [];
    var zuordnungen = [];
    var lpSumme;
    var akzeptierteLpSumme;


    this.init= function(_vorname, _nachname, _adresse, _matrikelNummer, _email, _land, _alterStudiengang, _studiengang, _notenSystem, _einstufungFachsemester){
      // initialize the pastry
      vorname= _vorname;
      nachname= _nachname;
      adresse= _adresse,
      matrikelNummer= _matrikelNummer;
      email= _email;
      land= _land;
      alterStudiengang= _alterStudiengang;
      studiengang= _studiengang;
      notenSystem= _notenSystem;
      einstufungFachsemester= _einstufungFachsemester;
      lpSumme = 0;
      akzeptierteLpSumme =0;
    }

  // Getter und Setter.

    this.getVorname= function(){
      return vorname;
    }
    this.setVorname= function(_vorname){
      vorname = _vorname;
    }


    this.getNachname= function(){
      return nachname;
    }
    this.setNachname= function(_nachname){
      nachname = _nachname;
    }

    this.getAdresse= function(){
      return adresse;
    }
    this.setAdresse= function(_adresse){
      adresse = _adresse;
    }

    this.getMatrikelNummer= function(){
      return matrikelNummer;
    }
    this.setMatrikelNummer= function(_matrikelNummer){
      matrikelNummer = _matrikelNummer;
    }


    this.getEmail= function(){
      return email;
    }
    this.setEmail= function(_email){
     email = _email;
    }


    this.getLand= function(){
      return land;
    }
    this.setLand= function(_land){
      land = _land;
    }
    this.getAlterStudiengang= function(){
      return alterStudiengang;
    }
    this.setAlterStudiengang= function(_alterStudiengang){
      alterStudiengang = _alterStudiengang;
    }


    this.getStudiengang= function(){
      return studiengang;
    }
    this.setStudiengang= function(_studiengang){
      studiengang = _studiengang;
    }


    this.getNotenSystem= function(){
      return notenSystem;
    }
    this.setNotenSystem= function(_notenSystem){
      notenSystem = _notenSystem;
    }
    this.getEinstufungFachsemester= function(){
      return einstufungFachsemester;
    }
    this.setEinstufungFachsemester= function(_einstufungFachsemester){
      einstufungFachsemester = _einstufungFachsemester;
    }


    this.getBelegteModule= function(){
      return belegteModule
    }
    this.setBelegteModule= function(_belegteModule){
      belegteModule = _belegteModule;
    }

    this.getLpSumme= function(){
      return lpSumme;
    }
    this.setLpSumme= function(_lpSumme){
      lpSumme = _lpSumme;
    }

    this.getAkzeptierteLpSumme= function(){
      return akzeptierteLpSumme;
    }
    this.setAkzeptierteLpSumme= function(_akzeptierteLpSumme){
      akzeptierteLpSumme = _akzeptierteLpSumme;
    }


    this.getZuordnungen= function(){
      return zuordnungen ;
    }
    this.setZuordnungen= function(_zuordnungen){
      zuordnungen = _zuordnungen;
    }

    this.getBelegtesModul= function(id){
      for(var i=0; i< belegteModule.length; i++ ){
        if(belegteModule[i].id===id){
          return belegteModule[i];  
        }
      }
      return null;
    }




    this.getJson = function(){
      //return this;
      json = {};
      json.vorname =vorname;
      json.nachname =nachname;
      json.adresse = adresse;
      json.matrikelNummer  =matrikelNummer;
      json.email = email;
      json.land = land; 
      json.alterStudiengang = alterStudiengang;
      json.studiengang = studiengang;
      json.notenSystem = notenSystem;
      json.einstufungFachsemester = einstufungFachsemester;
      json.belegteModule = belegteModule;
      json.zuordnungen = zuordnungen;
      return json;
    }


    this.updateZuordnungen = function(id){
      for(var i=0; i<zuordnungen.length; i++){
        zuordnungen[i].deleteModul(id);
      }
    }

}
