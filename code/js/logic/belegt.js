
function BelegtesModul(modul, note, lp, anzahlVesuche){
    this.modul = modul;
    this.note = note;
    this.lp = lp;
    this.anzahlVersuche = anzahlVesuche;
    var umgerechneteNote;
    this.id;



    this.init= function(id){
        //if(BelegtesModul.count=== undefined){
         //  BelegtesModul.count = 1;
        //}
        this.id = id;
   }

   this.setUmgerechneteNote= function(selectedNoten){
       selectedNoten !== undefined ? umgerechneteNote = NotenUmrechnung(this.note,selectedNoten):null;
   }
   this.getUmgerechneteNote = function(){
        if (umgerechneteNote === undefined){
            // throw new Exception("umgerechneteNote ist nicht gesetzt");
        }
    return umgerechneteNote;
   }



   var NotenUmrechnung= function(eingegebeneNote, selectedNoten) {
       eingegebeneNote = "" +eingegebeneNote
       eingegebeneNote = eingegebeneNote.replace(",", ".");

       //Eine List mit alle Noten, die es an der Uni gibt.

       var values = Object.values(selectedNoten);

       //Eine List mit alle Noten, die es in einem bestimmten land gibt.

       var keys = Object.keys(selectedNoten);

       keys = keys.map(Number).sort(function (a, b) { return a - b });


       var EndNote = "";

       if (isNaN(keys[0])) {

           EndNote = ZeichenUmrechnug(eingegebeneNote, selectedNoten);
       } else {
           EndNote = ZahlUmrechnung(eingegebeneNote, selectedNoten, keys, values);
       }

       //EndNote = MarburgNotenUmrechnung(EndNote);

       return EndNote;


   }


   var ZeichenUmrechnug = function(eingegebeneNote, selectedNoten) {

       var Endnote = "";

       $.each(selectedNoten, function (Note1, Note2) {

           if (Note1 == eingegebeneNote) {

               Endnote = Note2;
               return false;
           }
       });

       return Endnote;
   }


   var ZahlUmrechnung= function(eingegebeneNote, selectedNoten, keys, values) {



       var Endnote = "";
       var ersteElm = keys[0];
       var letzteElm = keys[(keys.length) - 1];

       eingegebeneNote = parseFloat(eingegebeneNote);



       if (ersteElm <= eingegebeneNote && letzteElm >= eingegebeneNote) {

           $.each(selectedNoten, function (Note1, Note2) {

               var NextKey = (keys.indexOf(parseFloat(Note1))) + 1;
               var NextNote = keys[NextKey];

               if (Note1 == eingegebeneNote) {
                   Endnote = Note2;
                   return false;

               } else if (Note1 < eingegebeneNote && NextNote > eingegebeneNote) {

                   if ((eingegebeneNote - Note1) <= (NextNote - eingegebeneNote)) {

                       Endnote = Note2;
                   }
                   else {
                       Endnote = values[NextKey];
                   }
                   return false;
               }

           });
       }

       return Endnote;
   }


//    var  MarburgNotenUmrechnung=function(eingegebeneNote) {

//        var letzteNote = ""

//        switch (eingegebeneNote) {
//            case 1:
//                letzteNote = 15;
//                break;
//            case 1.3:
//                letzteNote = 13;
//                break;
//            case 1.7:
//                letzteNote = 12;
//                break;
//            case 2:
//                letzteNote = 11;
//                break;
//            case 2.3:
//                letzteNote = 10;
//                break;
//            case 2.7:
//                letzteNote = 9;
//                break;
//            case 3:
//                letzteNote = 8;
//                break;
//            case 3.3:
//                letzteNote = 7;
//                break;
//            case 3.7:
//                letzteNote = 6;
//                break;
//            case 4:
//                letzteNote = 5;

//        }

//        return letzteNote;

//    }






}
